## Synopsis

This project is an example to how I usually approach FE development.

Even if the task doesnt require so much effort I tend to present my work in the best way possible considering how important is to apply standards, especially in FE development.

## dependecies

1. node
2. bower
3. grunt

All this program must be install with -g flag  , run > npm install -g *

## Installation

1. run > npm install && bower install


## Development

1. run >  grunt serve
2. A local server is ready on port 9000, with livereload!


## Tests

Mocha is installed by default.

to run the tests

1. > grunt test

## Production

1. run > grunt build
2. take al the package in the folder dist

## Contributors

Lorenc Estrefi
